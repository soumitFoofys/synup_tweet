Rails.application.routes.draw do
  resources :retweets
  resources :favs
  resources :follows
  resources :tweets
  resources :profiles
  root to: 'welcome#index'

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  post 'user_follow/:user_id' , to: "follows#user_follow"
  post 'user_unfollow/:user_id' , to: "follows#user_unfollow"

  post 'like_tweet/:tweet_id' , to: "favs#like_tweet"
  post 'unlike_tweet/:tweet_id' , to: "favs#unlike_tweet"

  get 'retweet_new/:tweet_id' , to: "retweets#retweet_new"

  post 'tweet_retweet/:tweet_id' , to: "tweets#tweet_retweet"

end
