class WelcomeController < ApplicationController
  def index
    if !user_signed_in?
      redirect_to new_user_registration_path
    end
    if user_signed_in?
      if current_user.profile.nil?
        redirect_to new_profile_path
      end
    end
    if user_signed_in?
      @all_tweets =  Tweet.where(user_id:  current_user.profile.follows.pluck(:user_id).uniq).uniq
      # @all_tweets =  Tweet.all
      @tweets = current_user.tweets
    else
      @all_tweets =  Tweet.all
    end

    @users = User.where.not(id: current_user)

    @tweet = Tweet.new
  end
end
