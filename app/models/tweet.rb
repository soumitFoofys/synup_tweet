class Tweet < ApplicationRecord
  belongs_to :user

  has_many :favs
  has_many :comments

  mount_uploader :tweetimg, TweetimgUploader
end
