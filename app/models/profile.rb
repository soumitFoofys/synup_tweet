class Profile < ApplicationRecord
  belongs_to :user
  has_many :follows


  mount_uploader :avatar, AvatarUploader

end
