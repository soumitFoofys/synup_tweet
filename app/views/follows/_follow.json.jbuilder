json.extract! follow, :id, :profile_id, :user_id, :created_at, :updated_at
json.url follow_url(follow, format: :json)
