json.extract! profile, :id, :address, :age, :father_name, :mother_name, :qualification, :user_id, :created_at, :updated_at
json.url profile_url(profile, format: :json)
