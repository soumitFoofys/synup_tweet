class CreateRetweets < ActiveRecord::Migration[5.1]
  def change
    create_table :retweets do |t|
      t.belongs_to :tweet, foreign_key: true
      t.text :message

      t.timestamps
    end
  end
end
