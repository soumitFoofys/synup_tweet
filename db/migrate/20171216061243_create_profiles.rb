class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.text :address
      t.integer :age
      t.string :father_name
      t.string :mother_name
      t.text :qualification
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
